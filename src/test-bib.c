#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>
#include "bib.h"

/** record di prova (corretti) */
char * record[]= {
  "autore : Di Ciccio   , Antonio; titolo: Manuale di architettura pisana; editore: Palestro; anno: 1990; luogo_pubblicazione: Milano; prestito: 10-02-2012; collocazione: B.23.4;descrizione_fisica: 123 p., ill,  23 cm;",
  "autore: Melis, Antonio Pietro Angelo; autore: Gatto Magnolfi, Gianni;  titolo:Manuale di architettura fiorentina; editore: EQR; anno: 1910; nota:  Copia del Vescovo; collocazione: Z.12.56; luogo_pubblicazione: Firenze; descrizione_fisica: 34 p., cov,  59 cm;",
  "autore: Kernighan, Brian W.; autore: Ritchie, Dennis M.;  titolo: Il linguaggio C (seconda Edizione); editore: Jackson Libri; anno: 1989; nota: Edizione italiana; collocazione: Z.22.56; luogo_pubblicazione: Milano; descrizione_fisica: 359 p., softcov,  13 cm;",  
"autore: Kernighan, Brian W.; autore: Pike, Rob;  titolo: Programmazione nella Pratica; editore: Addison-Wesley Longman Italia Editoriale; anno: 1999; nota: Edizione italiana; collocazione: Z.24.56; luogo_pubblicazione: Milano; descrizione_fisica: 294 p., softcov,  13 cm; prestito: 13-07-2012;",  
"autore: Singh, Jaswinder Pal; autore: Culler, David E.;  autore: Gupta,Anoop; titolo: Parallel Computer Architecture: A Hardware/Software Approach; editore: Morgan Kaufmann; anno: 1999; nota:  Copia M. Fillo; collocazione: QA76 58; luogo_pubblicazione: San Francisco, CA; descrizione_fisica: 1021 p., cov, ill,  23 cm;",  
"autore: Hennessy, John L.; autore: Patterson, David A.;  titolo: Computer Architecture, Fifth Edition: A Quantitative Approach   ; editore:  Morgan Kaufmann; anno: 2011; nota: ; collocazione: HHH.56; luogo_pubblicazione: San Francisco, CA; descrizione_fisica: 896 p., softcov, 7 1/2 X 9 1/4 inc;",  
"autore: Luccio, Fabrizio; autore: Pagli, Linda;  autore: Steel, Graham;titolo:Mathematical and Algorithmic Foundations of the Internet ; editore: CRC Press, Taylor and Francis Group; anno: 2011; nota: Chapman & Hall/CRC Applied Algorithms and Data Structures series; collocazione: Z.DDf.56; luogo_pubblicazione: New York; descrizione_fisica: 434 p., softcovcov,  22 cm;",
  NULL
};

/** record di prova (scorretti) */
char * badrecord[]= {
  "autore : Di Ciccio   , Antonio; titolo: Manuale di architettura pisana; editore: Palestro; anno: 1990; luogo_pubblicazione: Milano; prestito: 10-0-2012; collocazione: B.23.4;descrizione_fisica: 123 p., ill,  23 cm;",
  "autore: Melis, Antonio Pietro Angelo; autore: Gatto Magnolfi, Gianni;  titolo:Manuale di architettura fiorentina; editore: EQR; anno: 1910; nota:  Copia del Vescovo; collocazione: Z;12.56; luogo_pubblicazione: Firenze; descrizione_fisica: 34 p., cov,  59 cm;",
  "autore: Melis, Antonio Pietro Angelo; autore:, Gianni;  titolo:Manuale di architettura fiorentina; editore: EQR; anno: 1910; nota:  Copia del Vescovo; collocazione: Z.12.56; luogo_pubblicazione: Firenze; descrizione_fisica: 34 p., cov,  59 cm;",  
"autore: Singh, Jaswinder Pal; autore: Culler, David E.;  autore: Gupta,Anoop; titolo: Parallel Computer Architecture: A Hardware/Software Approach; editore: Morgan Kaufmann; 1999; nota:  Copia M. Fillo; collocazione: QA76 58; luogo_pubblicazione: San Francisco, CA; descrizione_fisica: 1021 p., cov, ill,  23 cm;",  
"autore: Hennessy, John L.; autore: Patterson, David A.;  titolo: Computer Architecture, Fifth Edition: A Quantitative Approach   ; editore:  Morgan Kaufmann; anno: 2011; nota: ; collocazione: HHH.56; luogo_pubblicazione: San Francisco, CA; descrizione_fisica: 896 p.; softcov, 7 1/2 X 9 1/4 inc;",  
"autore: Luccio, Fabrizio; autore: Pagli, Linda;  autore: Steel, Graham;titolo:Mathematical and Algorithmic Foundations of the Internet ; editore: CRC Press, Taylor and Francis Group; anno: 2011; nota: Chapman & Hall/CRC Applied Algorithms and Data Structures series; collocazione: Z.DDf.56; luogo_pubblicazione: New York; descrizione_fisica: 434 p., softcovcov,  22 cm",
  NULL
};

int main (void) {
  int i;
  scheda_t* s = NULL;
  char* c = NULL, *d = NULL;


  /* traccia la memoria allocata e deallocata */
  mtrace();

  /* alloca scheda vuota */
  s = new_scheda();

  /* allocata ? */
  if ( s == NULL ) 
    exit(EXIT_FAILURE);

  /* stampa scheda vuota */
  print_scheda(stdout,s);

  /* libera memoria */
  free_scheda(&s);
  if ( s != NULL ) 
    exit(EXIT_FAILURE);

  /* casi limite*/
  free_scheda(NULL);
  free_scheda(&s);


  /* prime conversioni */
  for (i=0; record[i]!=NULL; i++) {
    /* fprintf(stderr,"-- %s --\n",record[i]);*/
    if ( ( s = record_to_scheda(record[i],strlen(record[i])+1)) == NULL ) {
      fprintf(stderr,"Conversione Fallita!\n");
      exit(EXIT_FAILURE);
    }
    print_scheda(stdout,s);

    free_scheda(&s);
    
  }

  fprintf(stdout,"Fine primo controllo!\n");

  /* controllo coversioni da record a scheda e viceversa */
  for (i=0; record[i]!=NULL; i++) {
    /* fprintf(stderr,"-- %s --\n",record[i]);*/
    if ( ( s = record_to_scheda(record[i],strlen(record[i])+1)) == NULL ) {
      fprintf(stderr,"Conversione Fallita2!\n");
      exit(EXIT_FAILURE);
    }

    /* conversione inversa */
    if ( ( c = scheda_to_record(s) ) == NULL ) {
      fprintf(stderr,"Conversione Fallita3!\n");
      exit(EXIT_FAILURE);
    }
    free_scheda(&s);
    

    if ( ( s = record_to_scheda(c,strlen(c)+1)) == NULL ) {
      fprintf(stderr,"Conversione Fallita4!\n");
      exit(EXIT_FAILURE);
    }
    /* conversione inversa */
    if ( ( d = scheda_to_record(s) ) == NULL ) {
      fprintf(stderr,"Conversione Fallita5!\n");
      exit(EXIT_FAILURE);
    }
    
    /* la stampa deve essere uguale alla precedente ....*/
    print_scheda(stdout,s);
    free_scheda(&s);

    if ( strcmp(c,d) != 0 )  {
      fprintf(stderr,"Conversione Fallita6!\n");
      exit(EXIT_FAILURE);
    }

    free(c);
    free(d);
  }



  fprintf(stdout,"Fine secondo controllo!\n");

  /* alcune conversioni di record mal formattati*/
  for (i=0; badrecord[i]!=NULL; i++) {
    /* fprintf(stderr,"-- %s --\n",record[i]);*/
    if ( ( s = record_to_scheda(badrecord[i],strlen(badrecord[i])+1)) != NULL ) {
      fprintf(stderr,"Conversione Fallita7!\n");
      exit(EXIT_FAILURE);
    }
  }

  fprintf(stdout,"Fine terzo controllo!\n");

  /* fine traccia */
  muntrace();

  return EXIT_SUCCESS;
}
