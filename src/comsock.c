
#include "comsock.h"

#include <stdio.h>
#include <errno.h>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#define NQUEUE 10

/** Crea una socket AF_UNIX
 *  \param  path pathname della socket
 *
 *  \retval s    il file descriptor della socket  (s>0)
 *  \retval -1   in altri casi di errore (setta errno)
 *               errno = E2BIG se il nome eccede UNIX_PATH_MAX
 *
 *  in caso di errore ripristina la situazione inziale: rimuove eventuali socket create e chiude eventuali file descriptor rimasti aperti
 */
int createServerChannel(char* path) {
	int fd = -1;
	struct sockaddr_un s;

	if(path == NULL || path[0] == '\0') {
		errno = EINVAL;
		goto err;
	}

	if(strnlen(path, UNIX_PATH_MAX) >= UNIX_PATH_MAX) {
		errno = E2BIG;
		goto err;
	}

	unlink(path);
	fd = socket(AF_LOCAL, SOCK_STREAM, 0);

	s.sun_family = AF_UNIX;
	strcpy(s.sun_path, path);

	if(bind(fd, (struct sockaddr *) &s, sizeof(struct sockaddr_un)) == -1)
		goto err;

	if(listen(fd, NQUEUE) == -1)
		goto err;

	return fd;

	err: {
		int save_errno = errno;
		unlink(path);
		if(fd != -1)
			close(fd);
		errno = save_errno;
	}

	return -1;
}

/** Chiude un canale lato server (rimuovendo la socket dal file system)
 *   \param path path della socket
 *   \param s file descriptor della socket
 *
 *   \retval 0  se tutto ok,
 *   \retval -1  se errore (setta errno)
 */
int closeServerChannel(char* path, int s) {
	close(s);

	unlink(path);

	return 0;
}

/** accetta una connessione da parte di un client
 *  \param  s socket su cui ci mettiamo in attesa di accettare la connessione
 *
 *  \retval  c il descrittore della socket su cui siamo connessi
 *  \retval  -1 in casi di errore (setta errno)
 */
int acceptConnection(int s) {
	int fd = -1;
	fd = accept(s, NULL, NULL);
	return fd > 0 ? fd : -1;
}

/** legge un messaggio dalla socket --- attenzione si richiede che il messaggio sia adeguatamente spacchettato e trasferito nella struttura msg
 *  \param  sc  file descriptor della socket
 *  \param msg  indirizzo della struttura che conterra' il messagio letto
 *		(deve essere allocata all'esterno della funzione)
 *
 *  \retval lung  lunghezza del buffer letto, se OK
 *  \retval  -1   in caso di errore (setta errno)
 *                 errno = ENOTCONN se il peer ha chiuso la connessione
 *                   (non ci sono piu' scrittori sulla socket)
 *
 */
int receiveMessage(int sc, message_t * msg) {
	int l;

	fprintf(stderr, "receiveMessage: waiting header ...\n");

	/* receive header */
	l = recv(sc, msg, sizeof(*msg) - sizeof(msg->buffer), 0);

	if(l <= 0)
		goto done;

	fprintf(stderr, "receiveMessage: received header <%c><%d> ...\n", msg->type, msg->length);

	if(msg->length > 0) {
		/* receive payload, if any */

		fprintf(stderr, "receiveMessage: waiting payload ...\n");

		l = recv(sc, ((void*)msg) + l, msg->length, 0);
		if(l <= 0)
			goto done;

		if(msg->length < MAXBUF)
			/* Terminate buf with NUL, for display purpose only */
			msg->buffer[msg->length] = '\0';

		fprintf(stderr, "receiveMessage: received payload <%c><%d><%s>\n", msg->type, msg->length, msg->buffer);
	}

	done:

	if(l == 0) {
		fprintf(stderr, "receiveMessage: connection closed\n");
		errno = ENOTCONN;
		return -1;
	}

	if(l < 0)
		perror("receiveMessage");

	return l;
}

/** scrive un messaggio sulla socket --- attenzione devono essere inviati SOLO i byte significativi del campo buffer (msg->length byte) --  si richiede che il messaggio venga scritto con un'unica write dopo averlo adeguatamente impacchettato
 *   \param  sc file descriptor della socket
 *   \param msg indirizzo della struttura che contiene il messaggio da scrivere
 *
 *   \retval  n    il numero di caratteri inviati (se scrittura OK)
 *   \retval -1   in caso di errore (setta errno)
 *                 errno = ENOTCONN se il peer ha chiuso la connessione
 *                   (non ci sono piu' lettori sulla socket)
 */
int sendMessage(int sc, message_t *msg) {
	ssize_t s;

	if(msg->length < MAXBUF-1)
		/* NUL-terminate buffer for display purposes only */
		msg->buffer[msg->length] = '\0';

	fprintf(stderr, "sendMessage: sending <%c><%d><%s>\n", msg->type, msg->length, msg->buffer);

	/* Compute the total size of the packet */
	s = sizeof(*msg) - sizeof(msg->buffer) + msg->length;

	return send(sc, msg, s, 0);
}

/** crea una connessione all socket del server. In caso di errore funzione ritenta ntrial volte la connessione (a distanza di k secondi l'una dall'altra) prima di ritornare errore.
 *   \param  path  nome del socket su cui il server accetta le connessioni
 *   \param  ntrial numeri di tentativi prima di restituire errore (ntrial <=MAXTRIAL)
 *   \param  k secondi l'uno dell'altro (k <=MAXSEC)
 *
 *   \return fd il file descriptor della connessione
 *            se la connessione ha successo
 *   \retval -1 in caso di errore (setta errno)
 *               errno = E2BIG se il nome eccede UNIX_PATH_MAX
 *
 *  in caso di errore ripristina la situazione inziale: rimuove eventuali socket create e chiude eventuali file descriptor rimasti aperti
 */
int openConnection(char* path, int ntrial, int k) {
	int fd = -1;
	int i;
	struct sockaddr_un s;

	if(path == NULL || path[0] == '\0'
			|| ntrial < 0 || ntrial > MAXTRIAL
			|| k < 0 || k > MAXSEC) {
		errno = EINVAL;
		goto err;
	}

	if(strnlen(path, UNIX_PATH_MAX) >= UNIX_PATH_MAX) {
		errno = E2BIG;
		goto err;
	}

	fd = socket(AF_LOCAL, SOCK_STREAM, 0);

	s.sun_family = AF_UNIX;
	strcpy(s.sun_path, path);

	for(i = 0; i < ntrial; i++) {
		fprintf(stderr, "openConnection: connection attempt %d ... ", i+1);
		if(connect(fd, (struct sockaddr *) &s, sizeof(struct sockaddr_un)) != -1) {
			fprintf(stderr, "successful!\n");
			break;
		}
		fprintf(stderr, "failed! (Waiting for %d seconds...)\n", k);
		sleep(k);
	}

	if(i == ntrial)
		goto err;

	return fd;

	err:

	if(fd != -1) {
		int save_errno = errno;
		close(fd);
		errno = save_errno;
	}

	return -1;
}

/** Chiude una connessione
 *   \param s file descriptor della socket relativa alla connessione
 *
 *   \retval 0  se tutto ok,
 *   \retval -1  se errore (setta errno)
 */
int closeConnection(int s) {
	return close(s);
}

