#include "util.h"
#include "bib.h"
#include "comsock.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <pthread.h>
#include <signal.h>
#include <time.h>

#define LFILENAME 256
#define MAXSCHEDE

char *name_bib;
FILE *log_file, *record_file;
int socket_fd;

volatile sig_atomic_t terminating = FALSE;

int nschede;
scheda_t **schede;
pthread_mutex_t schede_mutex = PTHREAD_MUTEX_INITIALIZER;

typedef struct worker {
	pthread_t thread;
	int conn_fd;
	struct worker *prev;
	struct worker *next;
} worker_t;

pthread_mutex_t workers_mutex = PTHREAD_MUTEX_INITIALIZER;
worker_t* workers_list;

void catch_sig(int sig) {
	psignal(sig, "Caught signal");
	terminating = TRUE;
}

void setup_signal(int sig) {
	struct sigaction sa;

	sigaction(sig, NULL, &sa);
	sa.sa_handler = catch_sig;
	sigaction(sig, &sa, NULL );
}

void send_error_and_die(int conn_fd, char* error) {
	message_t msg;
	msg.type = MSG_ERROR;
	msg.length = strlen(error) + 1;
	strcpy(msg.buffer, error);

	fprintf(stderr, "Client error: %s\n", error);

	sendMessage(conn_fd, &msg);

	pthread_exit(NULL );
}

int scheda_matches(scheda_t *scheda, char *campi_query[NCAMPI]) {
	int ret = TRUE;
	char *strings[NCAMPI];
	campo_t campo;

	map_scheda_strings(scheda, strings);

	for (campo = 0; campo < NCAMPI; campo++) {
		if (campi_query[campo] == NULL )
			continue;

		fprintf(stderr, "worker: matching '%s: %s' ...\n", campo_tags[campo],
				campi_query[campo]);

		switch (campo) {
		int anno;
		autore_t *autore;
	case ANNO:
		anno = strtol(campi_query[campo], NULL, 10);
		if (anno != scheda->pub.anno)
			ret = FALSE;
		break;
	case AUTORE:
		ret = FALSE;
		for (autore = scheda->autore; autore != NULL ; autore = autore->next) {
			if (strstr(autore->nome, campi_query[campo])
					|| strstr(autore->cognome, campi_query[campo]))
				ret = TRUE;
		}
		break;
	default:
		if (strstr(strings[campo], campi_query[campo]) == NULL )
			ret = FALSE;
		break;
		}

		if (!ret)
			break;
	}

	fprintf(stderr, "worker: matching result: %s\n",
			ret ? "MATCHING" : "NOT matching");

	return ret;
}

void *worker_routine(void* param) {
	worker_t *me;
	message_t msg;
	sigset_t sigset;
	char campi_query_buf[NCAMPI][LLSTRING];
	char *campi_query[NCAMPI];
	char *cur;
	int i;
	int results;
	int loan;

	me = param;

	/* Ignore signals to be sure to complete */
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGTERM);
	sigaddset(&sigset, SIGINT);
	pthread_sigmask(SIG_BLOCK, &sigset, NULL );

	fprintf(stderr, "worker: starting (conn fd: %d)\n", me->conn_fd);

	if (receiveMessage(me->conn_fd, &msg) <= 0) {
		perror("receiveMessage");
		pthread_exit(NULL );
	}

	fprintf(stderr, "worker: received <%c><%d><%s>\n", msg.type, msg.length,
			msg.buffer);

	switch (msg.type) {
	case MSG_QUERY:
		loan = FALSE;
		break;
	case MSG_LOAN:
		loan = TRUE;
		break;
	default:
		send_error_and_die(me->conn_fd, "Invalid message received");
		break;
	}

	/* parse query */
	memset(campi_query, 0, sizeof(campi_query));
	cur = msg.buffer;
	for (;;) {
		char tag[LTAG+1], value[LLSTRING+1];
		int len;
		campo_t campo;

		while (isspace(*cur) && cur - msg.buffer < msg.length)
			cur++;
		if (*cur == '\0' || cur - msg.buffer >= msg.length)
			break;

		fprintf(stderr, "worker: parse query: parsing %20s...\n", cur);

		if((len = parse_value(cur, tag, value)) <= 0) {
			char s[LLSTRING];
			sprintf(s, "Unable to parse: %20s", cur);
			send_error_and_die(me->conn_fd, s);
		}

		if((campo = parse_campo(tag)) == NONE) {
			char s[LLSTRING];
			sprintf(s, "Unable to parse campo: %20s", tag);
			send_error_and_die(me->conn_fd, s);
		}

		strcpy(campi_query_buf[campo], value);
		campi_query[campo] = campi_query_buf[campo];

		cur += len;
	}

	fprintf(stderr, "worker: query parsing complete\n");

	fprintf(stderr, "worker: acquiring lock ...\n");
	pthread_mutex_lock(&schede_mutex);
	fprintf(stderr, "worker: lock acquired ...\n");

	results = 0;
	for (i = 0; i < nschede; i++) {
		char *record;

		fprintf(stderr, "worker: matching scheda %d/%d ...\n", i + 1, nschede);

		if (!scheda_matches(schede[i], campi_query))
			continue;

		if (loan) {
			struct tm *broken_time, *scad;
			time_t t;
			int oldday;
			char s[LDATE + 1];

			if (!schede[i]->prestito.disponibile) {
				fprintf(stderr, "worker: prestito non disponibile! \n");
				continue;
			}

			schede[i]->prestito.disponibile = FALSE;

			t = time(NULL );
			broken_time = gmtime(&t);
			scad = &schede[i]->prestito.scadenza;

			scad->tm_hour = scad->tm_min = scad->tm_sec = 0;
			scad->tm_zone = NULL;
			scad->tm_gmtoff = scad->tm_isdst = 0;

			scad->tm_year = broken_time->tm_year;
			scad->tm_mon = broken_time->tm_mon + 2;
			oldday = scad->tm_mday = broken_time->tm_mday;

			/* fix invalid dates */
			mktime(scad);
			/* fix case: 30-31 Feb */
			if (scad->tm_mday < oldday)
				scad->tm_mday = 1;
			mktime(scad);

			format_date(s, scad->tm_mday, scad->tm_mon + 1, scad->tm_year);
			fprintf(stderr, "worker: prestito accordato (scadenza: %s)\n", s);
		}

		fprintf(stderr, "worker: scheda %d matches!\n", i);

		record = scheda_to_record(schede[i]);

		if (results == 0) {
			/* first result, send MSG_OK first */
			msg.type = MSG_OK;
			msg.length = 0;
			fprintf(stderr, "worker: sending OK message\n");
			sendMessage(me->conn_fd, &msg);
		}

		msg.type = MSG_RECORD;
		msg.length = strlen(record) + 1;
		strcpy(msg.buffer, record);

		fprintf(stderr, "worker: sending record %s\n", msg.buffer);
		fprintf(log_file, "%s\n", msg.buffer);
		sendMessage(me->conn_fd, &msg);

		free(record);

		results++;
	}

	pthread_mutex_unlock(&schede_mutex);
	fprintf(stderr, "worker: lock released\n");

	if (results == 0) {
		msg.type = MSG_NO;
		msg.length = 0;
		fprintf(stderr, "worker: sending no records message\n");
		sendMessage(me->conn_fd, &msg);
	}

	fprintf(stderr, "worker: logging %s %d\n", loan ? "LOAN" : "QUERY",
			results);
	fprintf(log_file, "%s %d\n", loan ? "LOAN" : "QUERY", results);

	closeConnection(me->conn_fd);

	pthread_mutex_lock(&workers_mutex);
	if (me->prev != NULL )
		me->prev->next = me->next;
	else
		workers_list = me->next;
	if (me->next != NULL )
		me->next->prev = me->prev;
	free(me);
	pthread_mutex_unlock(&workers_mutex);

	pthread_exit(NULL );

	/* Calm down compiler */
	return NULL ;
}

static void loop() {
	while (!terminating) {
		int conn_fd;
		worker_t *worker;

		fprintf(stderr, "bibserver: waiting for connections ...\n");

		conn_fd = acceptConnection(socket_fd);

		if (conn_fd < 0) {
			perror("acceptConnection");
			break;
		}

		fprintf(stderr,
				"bibserver: new connection accepted! starting worker ...\n");

		pthread_mutex_lock(&workers_mutex);
		worker = malloc(sizeof(worker_t));
		worker->conn_fd = conn_fd;
		worker->prev = NULL;
		worker->next = workers_list;
		if (workers_list != NULL )
			workers_list->prev = worker;
		workers_list = worker;
		pthread_mutex_unlock(&workers_mutex);

		pthread_create(&worker->thread, NULL, worker_routine, worker);

		fprintf(stderr, "bibserver: worker started!\n");
	}
}

static void terminate() {
	/* Waiting for worker threads to terminate */
	pthread_mutex_lock(&workers_mutex);
	while (workers_list != NULL ) {
		worker_t *worker = workers_list;

		fprintf(stderr, "bibserver: waiting for worker fd:%d ...\n",
				workers_list->conn_fd);

		/* avoid deadlock */
		pthread_mutex_unlock(&workers_mutex);
		pthread_join(worker->thread, NULL );
		pthread_mutex_lock(&workers_mutex);
	}
	pthread_mutex_unlock(&workers_mutex);
}

int main(int argc, char **argv) {
	char *record_file_name;
	char log_file_name[LFILENAME];
	char socket_name[LFILENAME];

	if (argc != 3)
		die("Wrong number of arguments");

	name_bib = argv[1];
	record_file_name = argv[2];

	sprintf(log_file_name, "%s.log", name_bib);
	sprintf(socket_name, "tmp/%s.sck", name_bib);

	log_file = fopen_or_die(log_file_name, "w",
			"bibserver: unable to open log file");
	record_file = fopen_or_die(record_file_name, "r",
			"bibserver: unable to open record file");

	if ((socket_fd = createServerChannel(socket_name)) == -1)
		perror_and_die(socket_name,
				"bibserver: unable to create server socket");

	fprintf(stderr, "bibserver: listening on %s\n", socket_name);

	nschede = load_records(record_file, &schede);

	setup_signal(SIGINT);
	setup_signal(SIGTERM);

	loop();

	fprintf(stderr, "bibserver: waiting for worker threads to terminate ...\n");

	terminate();

	record_file = fopen_or_die(record_file_name, "w",
			"Unable to open record file for writing");

	fprintf(stderr, "bibserver: saving records file\n");
	store_records(record_file, schede, nschede);

	fprintf(stderr, "bibserver: BYE!\n");

	pthread_exit(NULL );
}
