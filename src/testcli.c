/**  \file                                                             
 *   \brief codice client (test comsock)
 *                                                        
 *    \author lso12                                                            
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>

#include "comsock.h"

/** nome server socket */
#define SERVSOCK "./tmp/normalepi.sck"

/** numero secondi fra i tentativi di connessione */
#define SECS 1
/** numero di tentativi di connessione */
#define TRIALS 10

/** numero di test */
#define M 2

/** numero messaggi di test */
#define N 8

/** caratteri del tipo */
char ch [N] = "QFELLONK";

/** messaggi di testo */
char * text [N] = {
  "Ending", 
  "Not clear",
  "Taking time . . .",
  NULL,
  "This is correct",
  NULL,
  NULL, 
  "Fred",
};

/** Main **/
int main(int argc, char** argv)
{
  int sck;
  int i;
  message_t msg;

  /* memoria */
  mtrace();

  /* chiamate errate */
  fprintf(stderr,"testcli: -- tentativi errati di connessione con il server\n");    

  sck = openConnection(SERVSOCK,-1,TRIALS);
  perror("Open connection");

  if ( sck != -1 ) {
    exit(EXIT_FAILURE);
  }

  sck = openConnection(SERVSOCK,MAXSEC-1,MAXTRIAL+3);
  perror("Open connection");

  if ( sck != -1 ) {
    exit(EXIT_FAILURE);
  }


  /* chiamata corretta */
  fprintf(stderr,"testcli: -- tentativo di connessione con il server a --%s--\n",SERVSOCK);    
  
  sck = openConnection(SERVSOCK,TRIALS,SECS);
  if ( sck == -1 ) {
    perror("testcli: Open connection");
    exit(EXIT_FAILURE);
  }
  
  fprintf(stderr,"testcli: -- connessione aperta %d\n",sck);    
  
  for ( i = 0; i<N ; i++ ) {
    
    /* preparo il messaggio ...*/
    msg.type=ch[i];
    msg.length=0;
    memset(msg.buffer,'\0',MAXBUF);
    if ( text[i] !=NULL ) {
      msg.length=strlen(text[i])+1;
      strcpy(msg.buffer,text[i]);
    }
    /* lo invio */
    /* skt e' la connessione su cui mandare il msg */
    if ( sendMessage(sck,&msg) == -1 )  {
      perror("testcli: sendMessage");
      break;
    }
    fprintf(stdout,"testcli: invio <%c><%d><%s> \n",msg.type,msg.length,msg.buffer);
    /* leggo la risposta */
    if ( receiveMessage(sck,&msg)  == -1) {
      perror("testcli: receiveMessage");
      break;
    }

    /* stampo la risposta */
    fprintf(stdout,"testcli: recevo <%c><%d><%s> \n",msg.type,msg.length,msg.buffer);
  }
  
  /* test scritture consecutive */
  for ( i = 0; i<N ; i++ ) {
    
    /* preparo il messaggio ...*/
    msg.type=ch[i];
    msg.length=0;
    memset(msg.buffer,'\0',MAXBUF);
    if ( text[i] !=NULL ) {
      msg.length=strlen(text[i])+1;
      strcpy(msg.buffer,text[i]);
    }
    /* lo invio */
    /* skt e' la connessione su cui mandare il msg */
    if ( sendMessage(sck,&msg) == -1 )  {
      perror("testcli: sendMessage");
      break;
    }
    fprintf(stdout,"testcli: invio <%c><%d><%s> \n",msg.type,msg.length,msg.buffer);
  }
  
  while (receiveMessage(sck,&msg) != -1) {
    /* stampo la risposta */
    fprintf(stdout,"testcli: recevo <%c><%d><%s> \n",msg.type,msg.length,msg.buffer);
  }

  perror("testcli: receiveMessage");

  closeConnection(sck);

  muntrace();

  exit(EXIT_SUCCESS);
}
