/**  \file                                                                                                                 
 *   \brief codice server (test comsock)
 *                                                                                                                         
 *    \author lso12                                                                                                       
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mcheck.h>

#include "comsock.h"

/* numero test scritture e letture */
#define N 8

/** nome server socket */
#define MAINSOCK "./tmp/normalepi.sck"



/**
  stampa il messaggio in formato
  tipo :  stringa nel buffer \n

  \param msg puntatore al messaggio da stampare
*/
void  printMessage(message_t * msg) {
  if ( msg->length == 0 )  {
    printf("%c : <void>\n", msg->type);
    return;
  }
  printf("%c : %s\n", msg->type,msg->buffer);
}



/** Main **/
int main(int argc, char** argv)
{

  /* socket di ascolto */
  int sk, sk_c;
  /* messaggio in ingresso ed in uscita */
  message_t msg, msgr, msgarr[N];
  int i;

  /* memoria ? */
  mtrace();

  /* inizializzo i buffer */
  memset(&msg,'\0',MAXBUF);
  memset(&msgr,'\0',MAXBUF);


  /*
   * Creazione socket principale (errata)
   */
  fprintf(stderr,"testserver: -- Prima invocazione errata\n");    


  if ( createServerChannel("") > 0 ) {
    exit( EXIT_FAILURE );
  }

  /*
   * Creazione socket principale (errata)
   */
  fprintf(stderr,"testserver: -- Seconda invocazione errata\n");    
  
  if ( createServerChannel("qksetzf/gsock") > 0 ) {
    exit( EXIT_FAILURE );
  }

  /*
   * Creazione socket principale (errata)
   */
  fprintf(stderr,"testserver: -- Terza invocazione errata\n");    
  sk=createServerChannel("qksetzf/gsock/kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
  perror("testserv: Creazione server socket");

  if ( sk > 0 ) {
    exit( EXIT_FAILURE );
  }

 /*
   * Creazione socket principale
   */

  if ( (sk=createServerChannel(MAINSOCK)) <= 0 ) {
    perror("testserv: Creazione server socket");
    exit( EXIT_FAILURE );
  }

  fprintf(stderr,"testserver: -- Creato server socket\n");    


  fprintf(stderr,"testserv: -- main Attendo su accept\n");    
  
  sk_c= acceptConnection(sk);
    
  if ( sk_c == -1) {
    perror("testserv: acceptConnection");
    closeServerChannel(MAINSOCK,sk);
    exit(EXIT_FAILURE);
  }

  fprintf(stderr,"testserv: -- main accept terminata %d\n",sk_c);    
  
  /* 
     test su invio di messaggi singoli 
  */

  for ( i = 0; i<N ; i++ ) {
    if ( receiveMessage(sk_c,&msg)  == -1) {
     perror("testserv: receiveMssage");
     break;
    }
    /* stampa del messaggio */
    printMessage(&msg);
    
    /* creazione messaggio di risposta */
    msgr.type = MSG_QUERY;
    strcpy(msgr.buffer,"server: ");
    
    if (msg.length > 0) strcat(msgr.buffer,msg.buffer);
    msgr.length = strlen(msgr.buffer) +1;
    
    /* invio messaggio di risposta */
    if ( sendMessage(sk_c,&msgr) == -1 )  {
      perror("testcli: sendMessage");
      break;
    }
  }
  
  /*
    test su invio di N messaggi consecutivi
  */
  
  for ( i = 0; i<N ; i++ ) {
    if ( receiveMessage(sk_c,&msg)  == -1) {
     perror("testserv: receiveMessage");
     break;
    }
    /* stampa del messaggio */
    printMessage(&msg);
    
    /* creazione messaggio di risposta */
    msgarr[i].type = MSG_QUERY;
    strcpy(msgarr[i].buffer,"server: ");
    
    if (msg.length > 0) strcat(msgarr[i].buffer,msg.buffer);
    msgarr[i].length = strlen(msgarr[i].buffer) +1;
  }

  /* invio messaggi di risposta */

   for ( i = 0; i<N ; i++ ) {
    
    if ( sendMessage(sk_c,&msgarr[i]) == -1 )  {
      perror("testcli: sendMessage");
      break;
    }
   }

  /* chiude la connessione */
  closeConnection(sk_c);

  /* chiude la server socket */
  closeServerChannel(MAINSOCK,sk);


  /* memoria ... */
  muntrace();

  return EXIT_SUCCESS;
}

