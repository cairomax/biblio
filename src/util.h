#ifndef UTIL_H_
#define UTIL_H_

#define _STRINGIFY(p) #p
#define STRINGIFY(p) _STRINGIFY(p)

#include <stdio.h>

void die(const char *errormsg);

void perror_and_die(const char *prefix, const char *errormsg);

FILE *fopen_or_die(const char *path, const char *mode, const char* errormsg);

#endif /* UTIL_H_ */
