/**
 * \file
 * \author Massimo Cairo <cairomassimo@gmail.com>
 *
 * \brief Implementation of file bib.h
 */

#include "bib.h"
#include "util.h"

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

/**
 * Alloca e inizializza una nuova scheda.
 *
 * @return un puntatore alla scheda allocata, o NULL se l'allocazione fallisce (setta errno).
 */
scheda_t * new_scheda() {
	char *strings[NCAMPI];
	scheda_t *scheda = malloc(sizeof(scheda_t));
	campo_t campo;

	if (scheda == NULL)
		goto done;

	map_scheda_strings(scheda, strings);

	for(campo = 0; campo < NCAMPI; campo++) {
		if(strings[campo] == NULL)
			continue;
		strings[campo][0] = '\0';
	}

	scheda->pub.anno = 0;

	scheda->autore = NULL;
	scheda->prestito.disponibile = TRUE;

	done:
	return scheda;
}

/**
 * Libera la memoria allocata da una scheda.
 *
 * La scheda da liberare è individuata passando per referenza un puntatore all'area di memoria allocata.
 * Il valore del puntatore viene settato a NULL dopo la liberazione.
 *
 * Se il puntatore ha già valore NULL, nessuna operazione viene effettuata.
 * Se viene passata una referenza NULL, nessuna operazione viene effettuata.
 *
 * @param ps un puntatore al puntatore all'area di memoria che contiene la scheda.
 */
void free_scheda(scheda_t ** ps) {
	autore_t *autore = NULL;

	if (ps == NULL || *ps == NULL )
		return;

	autore = (*ps)->autore;
	while (autore != NULL) {
		autore_t *next;

		next = autore->next;
		free(autore);
		autore = next;
	}

	free(*ps);

	*ps = NULL;
}

void format_date(char *output, int day, int mon, int year) {
	/* last two digits, not Y2K safe :) */
	year %= 100;
	sprintf(output, "%02d-%02d-%02d", day, mon, year);
}

int parse_date(char *input, int *pday, int *pmon, int *pyear) {
	if(sscanf(input, "%2d-%2d-%2d", pday, pmon, pyear) < 3)
		return -1;

	/* Y2K heuristic */
	*pyear += 1900;
	if(*pyear < 1970)
		*pyear += 100;

	return 0;
}

/**
 * Verifica che due liste di autori contengano esattamente gli stessi elementi, nello stesso ordine.
 *
 * Il controllo è implementato con una strategia ricorsiva.
 * In primis, viene controllato che i primi elementi delle due liste siano uguali,
 * dopodiché si controlla ricorsivamente che le liste ottenute eliminando i primi elementi siano uguali.
 *
 * @param a1 prima lista
 * @param a2 seconda lista
 * @return TRUE se le due liste sono uguali, FALSE altrimenti
 */
int is_equal_author_list(autore_t * a1, autore_t * a2) {
	if (a1 == NULL || a2 == NULL ) {
		/* We are at the end of some of the two lists */
		if (a1 == a2)
			return TRUE;
		else
			return FALSE;
	}

	if (strcmp(a1->nome, a2->nome) != 0)
		return FALSE;

	if (strcmp(a1->cognome, a2->cognome) != 0)
		return FALSE;

	return is_equal_author_list(a1->next, a2->next);
}

void map_scheda_strings(scheda_t *scheda, char *strings[NCAMPI]) {
	campo_t campo;

	for(campo = 0; campo < NCAMPI; campo++) {
		switch (campo) {
		case TITOLO:
			strings[campo] = scheda->titolo;
			break;
		case EDITORE:
			strings[campo] = scheda->pub.editore;
			break;
		case LUOGO_PUBBLICAZIONE:
			strings[campo] = scheda->pub.luogo;
			break;
		case COLLOCAZIONE:
			strings[campo] = scheda->collocazione;
			break;
		case DESCRIZIONE_FISICA:
			strings[campo] = scheda->descrizione_fisica;
			break;
		case NOTA:
			strings[campo] = scheda->nota;
			break;
		default:
			/* ignore other fields */
			strings[campo] = NULL;
			break;
		}
	}
}

int is_equal_scheda(scheda_t * s1, scheda_t * s2) {
	campo_t campo;
	char *strings_to_compare[2][NCAMPI];

	if (s1 == NULL || s2 == NULL )
		return FALSE;

	map_scheda_strings(s1, strings_to_compare[0]);
	map_scheda_strings(s2, strings_to_compare[1]);

	if (!is_equal_author_list(s1->autore, s2->autore))
		return FALSE;

	for (campo = 0; campo < NCAMPI;
			campo++) {
		if(strings_to_compare[0][campo] == NULL)
			continue;
		if (strcmp(strings_to_compare[0][campo], strings_to_compare[1][campo]) != 0)
			return FALSE;
	}

	if (s1->prestito.disponibile != s2->prestito.disponibile)
		return FALSE;

	if (!s1->prestito.disponibile
			&& mktime(&s1->prestito.scadenza) != mktime(&s2->prestito.scadenza))
		return FALSE;

	if (s1->pub.anno != s2->pub.anno)
		return FALSE;

	return TRUE;
}

/** stampa la scheda in formato RECORD (vedi sopra) sullo stream specificato

 \param f stream di output
 \param s puntatore alla scheda
 */
void print_scheda(FILE* f, scheda_t * s) {
	char *p;

	if (f == NULL || s == NULL ) {
		errno = EINVAL;
		return;
	}

	p = scheda_to_record(s);
	printf("%s\n", p);
	free(p);
}

const char *campo_tags[NCAMPI] = { "autore", "titolo", "editore",
		"luogo_pubblicazione", "anno", "collocazione", "descrizione_fisica",
		"nota", "prestito" };

campo_t parse_campo(char *tag) {
	int i;
	for (i = 0; i < NCAMPI; i++)
		if (strcmp(campo_tags[i], tag) == 0)
			return i;
	return NONE;
}

autore_t* parse_autore(char *value) {
	autore_t *autore = NULL;
	int len;

	autore = malloc(sizeof(autore_t));

	if(sscanf(value, " %" STRINGIFY(LCOGN) "[^,] , %" STRINGIFY(LNOME) "[^;]",
			autore->cognome, autore->nome) < 2)
		goto inval;

	/* strip trailing whitespace */

	len = strlen(autore->cognome);
	while(isspace(autore->cognome[len-1])) {
		autore->cognome[len-1] = '\0';
		len--;
	}

	len = strlen(autore->nome);
	while(isspace(autore->nome[len-1])) {
		autore->nome[len-1] = '\0';
		len--;
	}

	autore->next = NULL;

	return autore;

	inval:

	if (autore != NULL )
		free(autore);

	errno = EINVAL;
	return NULL ;
}

int parse_value(char *s, char *tag, char *value) {
	char *cur;
	int len;

	cur = s;

	if(sscanf(cur, " %*c") == EOF)
		/* we have only whitespace left */
		goto end;

	/* parse tag */
	len = -1;
	sscanf(cur, " %" STRINGIFY(LTAG) "[^:;]: %n", tag, &len);
	if(len == -1)
		goto inval;
	cur += len;

	/* parse value */
	len = -1;
	sscanf(cur, "%" STRINGIFY(LLSTRING) "[^;]; %n", value, &len);
	if(len == -1) {
		/* allow value to be empty */
		value[0] = '\0';
		sscanf(cur, "; %n", &len);
	}
	if(len == -1)
		goto inval;
	cur += len;

	/* trim trailing whitespace */
	len = strlen(tag);
	while(isspace(tag[len-1]) && len > 0)
		tag[--len] = '\0';

	len = strlen(value);
	while(isspace(value[len-1]) && len > 0)
		value[--len] = '\0';

	return cur - s;

	inval:

	return -1;

	end:

	return 0;
}


/** trasforma un record in una scheda
 \param r record da trasformare (non viene modificato)
 \param l lunghezza massima del record (serve ad evitare l'overrun ...)

 \returns res la nuova scheda (allocata all'interno della funzione)
 \returns NULL se ci sono stati problemi (setta errno)
 errno=EINVAL per stringa mal formattata
 */
scheda_t * record_to_scheda(char* r, int l) {
	scheda_t *scheda;
	autore_t *last_autore = NULL;
	char *strings[NCAMPI];

	char *cur;

	cur = r;
	scheda = new_scheda();

	map_scheda_strings(scheda, strings);

	/* Per ogni campo */
	for (;;) {
		char tag[LTAG+1], value[LLSTRING+1];
		int len;
		campo_t campo;
		autore_t *autore;

		if((len = parse_value(cur, tag, value)) < 0)
			goto inval;
		if(len == 0)
			break;

		if((campo = parse_campo(tag)) == NONE)
			goto inval;

		switch (campo) {
			struct tm *scad;
			int day, mon, year;
			day = mon = year = -1; /* calm down valgrind */

		case AUTORE:
			if ((autore = parse_autore(value)) == NULL)
				goto inval;

			if (last_autore == NULL )
				scheda->autore = autore;
			else
				last_autore->next = autore;
			last_autore = autore;
			break;
		case ANNO:
			errno = 0;
			scheda->pub.anno = strtol(value, NULL, 10);
			if (errno != 0)
				goto inval;
			break;
		case PRESTITO:
			scheda->prestito.disponibile = FALSE;
			scad = &scheda->prestito.scadenza;

			if(parse_date(value, &day, &mon, &year) < 0)
				goto inval;
			mon--;

			scad->tm_hour = scad->tm_min = scad->tm_sec = 0;
			scad->tm_zone = NULL;
			scad->tm_gmtoff = 0;
			scad->tm_isdst = 0;

			scad->tm_mday = day;
			scad->tm_mon = mon;
			scad->tm_year = year;
			mktime(scad);

			if (scad->tm_mday != day || scad->tm_mon != mon
					|| scad->tm_year != year)
				goto inval;

			break;
		default:
			/* strings field */
			strcpy(strings[campo], value);
			break;
		}

		cur += len;
	}

	return scheda;

	inval:

	if (scheda != NULL )
		free_scheda(&scheda);

	errno = EINVAL;
	return NULL ;
}

/** trasforma una scheda in un record
 \param s la scheda da trasformare

 \returns r il puntatore al record che rappresenta la scheda (allocato all'interno della funzione)
 \returns  NULL se ci sono stati problemi (setta errno)
 */
char * scheda_to_record(scheda_t* scheda) {
	autore_t *autore;
	campo_t campo;
	char *strings[NCAMPI];

	char *buf, *cur;

	/* Alloca una stringa sufficientemente lunga. */
	cur = buf = malloc(LRECORD);

	/* Stampa il record nella stringa */

	map_scheda_strings(scheda, strings);

	for(campo = 0; campo < NCAMPI; campo++) {
		switch(campo) {
		case AUTORE:
			for (autore = scheda->autore; autore != NULL ; autore = autore->next) {
				cur += sprintf(cur, "autore: %s, %s; ", autore->cognome, autore->nome);
			}
			break;
		case PRESTITO:
			if (scheda->prestito.disponibile == FALSE) {
				struct tm *scad;
				char s[LDATE + 1];

				scad = &scheda->prestito.scadenza;

				format_date(s, scad->tm_mday, scad->tm_mon + 1, scad->tm_year);
				cur += sprintf(cur, "prestito: %s; ", s);
			}
			break;
		case ANNO:
			cur += sprintf(cur, "anno: %d; ", scheda->pub.anno);
			break;
		default:
			/* string field */
			cur += sprintf(cur, "%s: %s; ", campo_tags[campo], strings[campo]);
			break;
		}
	}

	/* Elimina lo spreco di memoria */

	buf = realloc(buf, strlen(buf) + 1);

	return buf;
}

/** legge tutti i record da un file e li memorizza in un array -- i record malformati vengono scartati ma se c'e' almeno un record convertibile corretamente la funzione ha successo

 \param fin stream da cui leggere i record
 \param psarray puntatore alla variabile che conterra' l'indirizzo dell'array di puntatori alle schede (allocato), significativo se n >0

 \returns -1 se si e' verificato un errore (setta errno)
 \returns n (n > 0) numero di record convertiti correttamente (ampiezza dell'array di schede) in questo caso *psarray contiene l'indirizzo dell'array allocato
 */
int load_records(FILE* fin, scheda_t** psarray[]) {
	int i;
	char buf[LRECORD];

	int size = 0;

	*psarray = NULL;

	i = 0;
	while (fgets(buf, LRECORD, fin) != NULL ) {
		scheda_t *scheda;

		if (buf[0] == '\n')
			continue;

		if (i >= size) {
			size = i * 2 + 1;
			*psarray = realloc(*psarray, sizeof(scheda_t*) * size);
		}

		scheda = record_to_scheda(buf, strlen(buf));

		if (scheda != NULL ) {
			(*psarray)[i] = scheda;
			i++;
		}
	}

	return i;
}

/** scrive su file un array di schede sotto forma di record
 \param fout stream su cui scrivere i record
 \param sarray puntatore all'array di puntatori alle schede
 \param n numero schede nell'array

 \returns -1 se si e' verificato un errore (setta errno) -- nota l'errore nella scrittura di un singolo record viene riportato nel numero di record scritti, non provoca il ritorno di (-1)
 \returns n (n > 0) numero di record scritti correttamente */
int store_records(FILE* fout, scheda_t* sarray[], int n) {
	int i;
	for (i = 0; i < n; i++) {
		char *r = scheda_to_record(sarray[i]);

		fprintf(fout, "%s\n", r);

		free(r);
	}

	return n;
}

typedef struct scheda_ref {
	scheda_t *s;
	campo_t c;
} scheda_ref_t;

int autore_comparator(autore_t *a, autore_t* b) {
	int ret;

	if (a == NULL && b == NULL )
		return 0;
	if (a == NULL )
		return -1;
	if (b == NULL )
		return +1;

	if ((ret = strcmp(a->cognome, b->cognome)) != 0
			|| (ret = strcmp(a->nome, b->nome)) != 0)
		return ret;

	return autore_comparator(a->next, b->next);
}

int scheda_ref_comparator(const void * aa, const void * bb) {
	const scheda_ref_t *a, *b;
	char *astr[NCAMPI], *bstr[NCAMPI];
	campo_t c;

	/* downcast */
	a = aa;
	b = bb;

	map_scheda_strings(a->s, astr);
	map_scheda_strings(b->s, bstr);

	/* type of comparator */
	c = a->c;

	switch (c) {
	case AUTORE:
		return autore_comparator(a->s->autore, b->s->autore);
	case ANNO:
		return a->s->pub.anno - b->s->pub.anno;
	case PRESTITO:
		return mktime(&a->s->prestito.scadenza)
				- mktime(&b->s->prestito.scadenza);
	default:
		return strcmp(astr[c], bstr[c]);
	}
}

/** ordina i record nell'array relativamente al campo specificato. Gli ordinamenti definiti per i vari campi sono i seguenti:
 AUTORE -- lessicografico (Cognome Nome) sulla lista degli autori
 TITOLO, EDITORE, LUOGO_PUBBLICAZIONE, COLLOCAZONE, DESCRIZIONE_FISICA, NOTA -- Lessicografico sulla stringa
 ANNO -- crescente
 PRESTITO -- crescente

 Nota: per ordine Lessicografico si intende quello normalmente usato nella strcmp()

 \param sarray puntatore array da ordinare (viene modificato durante l'ordinamento)
 \param n lunghezza array
 \param c campo rispetto al quale ordinare

 \returns -1 se si e' verificato un errore (setta errno) (in questo caso s e' invariato)
 \returns 0 se tutto e' andato bene
 */
int sort_schede(scheda_t* sarray[], int n, campo_t c) {
	scheda_ref_t *refs;
	int i;

	refs = malloc(sizeof(scheda_ref_t) * n);

	for (i = 0; i < n; i++) {
		refs[i].s = sarray[i];
		refs[i].c = c;
	}

	qsort(refs, n, sizeof(scheda_ref_t), scheda_ref_comparator);

	for (i = 0; i < n; i++) {
		sarray[i] = refs[i].s;
	}

	free(refs);

	return 0;
}
