#include "util.h"

#include <stdlib.h>
#include <stdio.h>

void die(const char *errormsg) {
	fprintf(stderr, "%s\n", errormsg);
	exit(1);
}

void perror_and_die(const char *prefix, const char *errormsg) {
	fprintf(stderr, "%s\n", errormsg);
	perror(prefix);
	exit(1);
}

FILE *fopen_or_die(const char *path, const char *mode,
		const char* errormsg) {
	FILE *file;
	if ((file = fopen(path, mode)) == NULL )
		perror_and_die(path, errormsg);
	return file;
}

