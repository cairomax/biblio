#include "util.h"
#include "bib.h"
#include "comsock.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define MAXBIBS 100
#define LBIBNAME 100
#define CONF_FILE_NAME "bib.conf"

char *query[NCAMPI];
int nbibs = 0;
char bibs[MAXBIBS][LBIBNAME + 1];
int loan = FALSE;

int countResults = 0;

void wrong_argument(char *arg) {
	fprintf(stderr, "Wrong argument: %s\n", arg);
	exit(1);
}

void parse_options(int argc, char **argv) {
	int i;

	if (argc < 2)
		die("Missing argument");

	for (i = 1; i < argc; i++) {
		char *arg;
		char tag[LTAG], value[LLSTRING];
		int len;
		campo_t campo;

		arg = argv[i];

		if (strcmp("-p", arg) == 0) {
			loan = TRUE;
			continue;
		}

		len = -1;
		sscanf(arg, "--%" STRINGIFY(LTAG) "[^=]=%n%" STRINGIFY(LLSTRING) "s",
				tag, &len, value);

		if (len == -1)
			wrong_argument(arg);
		if ((campo = parse_campo(tag)) == NONE)
			wrong_argument(arg);
		if (query[campo] != NULL)
			wrong_argument(arg);
		fprintf(stderr, "Parsed value: %s\n", value);

		query[campo] = malloc(strlen(value) + 1);
		strcpy(query[campo], value);
	}
}

void parse_conf_file() {
	FILE *f;
	char bibname[LBIBNAME + 1];

	f = fopen_or_die(CONF_FILE_NAME, "r", "Unable to open configuration file");

	for (nbibs = 0; fgets(bibname, LBIBNAME, f) != NULL ; nbibs++) {
		int len;

		if (nbibs >= MAXBIBS)
			die("Too many bibs");

		if ((len = strnlen(bibname, LBIBNAME + 2)) > LBIBNAME + 1)
			die("Bib name too long");

		if (bibname[len - 1] == '\n') {
			bibname[len - 1] = '\0';
			len--;
		}

		strcpy(bibs[nbibs], bibname);
	}
}

void perform_query(char *bibname) {
	char path[LBIBNAME + 10];
	int conn_fd;
	int i;
	message_t msg;
	char *cur;

	sprintf(path, "tmp/%s.sck", bibname);

	conn_fd = openConnection(path, NTRIAL, NSEC);

	if (conn_fd == -1)
		perror_and_die(path, "Failed to open connection");

	msg.type = loan ? MSG_LOAN : MSG_QUERY;

	cur = msg.buffer;
	for (i = 0; i < NCAMPI; i++) {
		if (query[i] == NULL )
			continue;

		cur += snprintf(cur, msg.buffer + MAXBUF - cur, "%s: %s; ",
				campo_tags[i], query[i]);

		if(cur == msg.buffer + MAXBUF)
			die("Query too big");
	}

	msg.length = cur - msg.buffer;

	if(sendMessage(conn_fd, &msg) < 0)
		perror_and_die("sendMessage", "Unable to send query message");

	if(receiveMessage(conn_fd, &msg) < 0)
		perror_and_die("receiveMessage", "Unable to receive response message");

	switch(msg.type) {
	case MSG_NO:
		return;
	case MSG_OK:
		break;
	}

	for(;;) {
		if(receiveMessage(conn_fd, &msg) < 0) {
			if(errno == ENOTCONN) {
				fprintf(stderr, "Connection closed\n");
				break;
			} else {
				perror_and_die("receiveMessage", "Error while receiving results");
			}
		}

		if(msg.type != MSG_RECORD) {
			fprintf(stderr, "Error from server: %s\n", msg.buffer);
			exit(1);
		}

		printf("%s\n", msg.buffer);
		countResults++;
	}

	closeConnection(conn_fd);
}

void perform_queries() {
	int i;
	for (i = 0; i < nbibs; i++) {
		perform_query(bibs[i]);
	}
	printf("%s %d\n", loan ? "LOAN" : "QUERY", countResults);
}

int main(int argc, char **argv) {
	int i;

	for (i = 0; i < NCAMPI; i++)
		query[i] = NULL;

	parse_options(argc, argv);
	parse_conf_file();
	perform_queries();

	return 0;
}
